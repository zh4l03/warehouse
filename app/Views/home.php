<?= include('layout/header.php') ?>
<!--start content-->
<main class="page-content">
  <!--breadcrumb-->
  <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
    <div class="breadcrumb-title pe-3">Dashboards</div>
    <div class="ps-3">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-0 p-0">
          <li class="breadcrumb-item"><a href="javascript:;"><i class="bi bi-house-door"></i></a>
          </li>
          <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
        </ol>
      </nav>
    </div>
  </div>
  <!--end breadcrumb-->

  <div class="row">
    <div class="col-12 col-lg-12 col-xl-12 d-flex">
      <div class="card radius-10 w-100">
        <div class="card-body">
          <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 row-cols-xl-3 row-cols-xxl-3 g-3">
            <div class="col">
              <label for="validationCustom04" class="form-label">Tempat Penyimpanan</label>
              <select class="form-select">
                <option value="">All</option>
                <option>Jakarta Barat</option>
                <option>Jakarta Timur</option>
              </select>
            </div>
            <div class="col">
              <label for="validationCustom04" class="form-label">Bulan</label>
              <select class="form-select">
                <option value="">All</option>
                <?php
                $bulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                $jlh_bln = count($bulan);
                for ($c = 0; $c < $jlh_bln; $c += 1) {
                  echo "<option value=$bulan[$c]> $bulan[$c] </option>";
                }
                ?>
              </select>
            </div>
            <div class="col">
              <label for="validationCustom04" class="form-label">Tahun</label>
              <select class="form-select">
                <option value="">All</option>
                <?php
                $now = date('Y');
                for ($a = $now; $a <= $now + 4; $a += 1) {
                  echo "<option value='$a'>$a</option>";
                }
                ?>
              </select>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-12 col-lg-12 col-xl-12 d-flex">
      <div class="card radius-10 w-100">
        <div class="card-body">
          <div class="col">
            <h5>Rangkuman Laporan <?php echo date('F Y'); ?></h5>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-12 col-lg-12 col-xl-6 d-flex">
      <div class="card radius-10 w-100">
        <div class="card-body">
          <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 row-cols-xl-3 row-cols-xxl-3 g-3">
            <div class="col">
              <div class="card radius-10 bg-tiffany mb-0">
                <div class="card-body text-center">
                  <div class="widget-icon mx-auto mb-3 bg-white-1 text-white">
                    <i class="bi bi-truck"></i>
                  </div>
                  <h3 class="text-white">25</h3>
                  <p class="mb-0 text-white">Jumlah<br>Pengiriman</p>
                </div>
              </div>
            </div>
            <div class="col">
              <div class="card radius-10 bg-danger mb-0">
                <div class="card-body text-center">
                  <div class="widget-icon mx-auto mb-3 bg-white-1 text-white">
                    <i class="lni lni-cart-full"></i>
                  </div>
                  <h3 class="text-white">35</h3>
                  <p class="mb-0 text-white">Jumlah<br>Fulfillment</p>
                </div>
              </div>
            </div>
            <div class="col">
              <div class="card radius-10 bg-success mb-0">
                <div class="card-body text-center">
                  <div class="widget-icon mx-auto mb-3 bg-white-1 text-white">
                    <i class="bx bx-book-content"></i>
                  </div>
                  <h3 class="text-white">16</h3>
                  <p class="mb-0 text-white">Inventaris<br>Tersimpan</p>
                </div>
              </div>
            </div>
            <div class="col">
              <div class="card radius-10 bg-dark mb-0">
                <div class="card-body text-center">
                  <div class="widget-icon mx-auto mb-3 bg-white-1 text-white">
                    <i class="lni lni-tshirt"></i>
                  </div>
                  <h3 class="text-white">18</h3>
                  <p class="mb-0 text-white">Produk<br>Terdaftar</p>
                </div>
              </div>
            </div>
            <div class="col">
              <div class="card radius-10 bg-purple mb-0">
                <div class="card-body text-center">
                  <div class="widget-icon mx-auto mb-3 bg-white-1 text-white">
                    <i class="bx bx-package"></i>
                  </div>
                  <h3 class="text-white">22</h3>
                  <p class="mb-0 text-white">Penyimpanan<br>Digunakan</p>
                </div>
              </div>
            </div>
            <div class="col">
              <div class="card radius-10 bg-orange mb-0">
                <div class="card-body text-center">
                  <div class="widget-icon mx-auto mb-3 bg-white-1 text-white">
                    <i class="bx bx-money"></i>
                  </div>
                  <h3 class="text-white">45</h3>
                  <p class="mb-0 text-white">Produk Terjual<br>(IKU)</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-12 col-lg-12 col-xl-6 d-flex">
      <div class="card radius-10 w-100">
        <div class="card-header bg-transparent">
          <div class="row g-3 align-items-center">
            <div class="col">
              <h6 class="mb-0">Laporan IKU Masuk & Keluar <?php echo date('F Y'); ?></h6>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div id="chart1"></div>
        </div>
      </div>
    </div>
  </div>
  <!--end row-->

  <div class="row">
    <div class="col-12 col-lg-12 col-xl-6 d-flex">
      <div class="card radius-10 w-100">
        <div class="card-header bg-transparent p-3">
          <div class="row row-cols-1 row-cols-lg-2 g-3 align-items-center">
            <div class="col">
              <h6 class="mb-0">Laporan IKU Tersimpan <?php echo date('F Y'); ?></h6>
            </div>
            <div class="col">
              <div class="d-flex align-items-center justify-content-sm-end gap-3 cursor-pointer">
                <div class="font-13"><i class="bi bi-circle-fill text-info"></i><span class="ms-2">Posts</span></div>
                <div class="font-13"><i class="bi bi-circle-fill text-orange"></i><span class="ms-2">Comments</span></div>
              </div>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div id="chart5"></div>
        </div>
      </div>
    </div>
    <div class="col-12 col-lg-12 col-xl-6 d-flex">
      <div class="card radius-10 w-100">
        <div class="card-header bg-transparent">
          <div class="row g-3 align-items-center">
            <div class="col">
              <h6 class="mb-0">Grafik Sisa Umur Simpan <?php echo date('F Y'); ?></h6>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div id="chart2"></div>
        </div>
      </div>
    </div>
  </div>
  <!--end row-->

</main>
<!--end page main-->

<?= include('layout/footer.php') ?>