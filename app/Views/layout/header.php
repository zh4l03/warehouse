<!doctype html>
<html lang="en" class="light-theme">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?= base_url('images/favicon-32x32.png') ?>" type="image/png" />

    <!--plugins-->
    <link href="<?= base_url('plugins/simplebar/css/simplebar.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('plugins/perfect-scrollbar/css/perfect-scrollbar.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('plugins/metismenu/css/metisMenu.min.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('plugins/select2/css/select2.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('plugins/select2/css/select2-bootstrap4.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('plugins/datatable/css/dataTables.bootstrap5.min.css') ?>" rel="stylesheet" />

    <!-- Bootstrap CSS -->
    <link href="<?= base_url('css/bootstrap.min.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('css/bootstrap-extended.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('css/style.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('css/icons.css') ?>" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

    <!-- loader-->
    <link href="<?= base_url('css/pace.min.css') ?>" rel="stylesheet" />


    <!--Theme Styles-->
    <link href="<?= base_url('css/dark-theme.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('css/light-theme.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('css/semi-dark.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('css/header-colors.css') ?>" rel="stylesheet" />

    <title>Warehouse System</title>
</head>

<body>

    <!--start wrapper-->
    <div class="wrapper">
        <!--start top header-->
        <header class="top-header">
            <nav class="navbar navbar-expand">
                <div class="mobile-toggle-icon d-xl-none">
                    <i class="bi bi-list"></i>
                </div>
                <div class="top-navbar-right ms-auto">
                    <ul class="navbar-nav align-items-center">
                        <li class="nav-item dropdown dropdown-large">
                            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="#" data-bs-toggle="dropdown">
                                <div class="user-setting d-flex align-items-center gap-1">
                                    <img src="<?= base_url('images/avatars/avatar-1.png') ?>" class="user-img" alt="">
                                    <div class="user-name d-none d-sm-block">Syamsul Rizal</div>
                                </div>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end">
                                <li>
                                    <a class="dropdown-item" href="#">
                                        <div class="d-flex align-items-center">
                                            <img src="<?= base_url('images/avatars/avatar-1.png') ?>" alt="" class="rounded-circle" width="60" height="60">
                                            <div class="ms-3">
                                                <h6 class="mb-0 dropdown-user-name">Syamsul Rizal</h6>
                                                <small class="mb-0 dropdown-user-designation text-secondary">Administrator</small>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>
                                <li>
                                    <a class="dropdown-item" href="pages-user-profile.html">
                                        <div class="d-flex align-items-center">
                                            <div class="setting-icon"><i class="bi bi-person-fill"></i></div>
                                            <div class="setting-text ms-3"><span>Ganti Password</span></div>
                                        </div>
                                    </a>
                                </li>
                                <!-- <li>
                                    <hr class="dropdown-divider">
                                </li> -->
                                <li>
                                    <a class="dropdown-item" href="authentication-signup-with-header-footer.html">
                                        <div class="d-flex align-items-center">
                                            <div class="setting-icon"><i class="bi bi-lock-fill"></i></div>
                                            <div class="setting-text ms-3"><span>Logout</span></div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown dropdown-large">
                            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="#" data-bs-toggle="dropdown">
                                <div class="user-setting d-flex align-items-center gap-1">
                                    &nbsp;&nbsp;<i class="bx bx-wallet-alt"></i>
                                    <div class="user-name d-none d-sm-block">Rp. 900.000,-</div>
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end">
                                <div class="row row-cols-1">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-body">
                                                <ul class="list-group">
                                                    <li class="list-group-item active" aria-current="true">
                                                        <h6><i class="bx bx-money"></i> Saldo : </h6>
                                                        <h5>Rp 900.000,-</h5>
                                                    </li>
                                                    <li class="list-group-item "></li>
                                                    <li class="list-group-item">Kredit Tersedia : Rp 500.000,-</li>
                                                    <li class="list-group-item">
                                                        <button type="button" class="btn btn-sm btn-success px-4"><i class="bx bx-money"></i> Top Up</button>
                                                        <button type="button" class="btn btn-sm btn-primary px-4"><i class="bx bx-history"></i> Riwayat</button>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end row-->
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--end top header-->

        <!--start sidebar -->
        <aside class="sidebar-wrapper" data-simplebar="true">
            <div class="sidebar-header">
                <div>
                    <img src="<?= base_url('images/logo-icon.png') ?>" class="logo-icon" alt="logo icon">
                </div>
                <div>
                    <h4 class="logo-text"><b>PT NCS</b></h4>
                </div>
                <div class="toggle-icon ms-auto"><i class="bi bi-chevron-double-left"></i>
                </div>
            </div>
            <!--navigation-->
            <ul class="metismenu" id="menu">
                <li>
                    <a href="<?= base_url() ?>">
                        <div class="parent-icon"><i class="bi bi-house-door"></i>
                        </div>
                        <div class="menu-title">Dashboard</div>
                    </a>
                </li>
                <li class="menu-label">Tenant Menu</li>
                <li>
                    <a href="javascript:;" class="has-arrow">
                        <div class="parent-icon"><i class="lni lni-tshirt"></i>
                        </div>
                        <div class="menu-title">Produk</div>
                    </a>
                    <ul>
                        <li> <a href="<?= base_url('produk') ?>"><i class="bi bi-arrow-right-short"></i>Produk Utama</a>
                        </li>
                        <li> <a href="#"><i class="bi bi-arrow-right-short"></i>Produk Bundling</a>
                        </li>
                        <li> <a href="#"><i class="bi bi-arrow-right-short"></i>Produk Subscription</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <div class="parent-icon"><i class="bi bi-truck"></i>
                        </div>
                        <div class="menu-title">Pengiriman</div>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" class="has-arrow">
                        <div class="parent-icon"><i class="bx bx-book-content"></i>
                        </div>
                        <div class="menu-title">Inventaris</div>
                    </a>
                    <ul>
                        <li> <a href="#"><i class="bi bi-arrow-right-short"></i>Inventaris IKU</a>
                        </li>
                        <li> <a href="#"><i class="bi bi-arrow-right-short"></i>Inventaris BKU</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow" href="javascript:;">
                        <div class="parent-icon"><i class="lni lni-cart-full"></i>
                        </div>
                        <div class="menu-title">Fulfillment</div>
                    </a>
                    <ul>
                        <li> <a href="component-alerts.html"><i class="bi bi-arrow-right-short"></i>Fulfillment Reguler</a>
                        </li>
                        <li> <a href="component-accordions.html"><i class="bi bi-arrow-right-short"></i>Fulfillment Subscription</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <div class="parent-icon"><i class="lni lni-customer"></i>
                        </div>
                        <div class="menu-title">Pengembalian</div>
                    </a>
                </li>
                <li>
                <li>
                    <a href="#">
                        <div class="parent-icon"><i class="lni lni-support"></i>
                        </div>
                        <div class="menu-title">Ganti SKU</div>
                    </a>
                </li>
                <li>
                <li>
                    <a href="#">
                        <div class="parent-icon"><i class="bx bx-package"></i>
                        </div>
                        <div class="menu-title">Peralihan</div>
                    </a>
                </li>
                <li class="menu-label">TOWS Menu</li>
                <li>
                    <a href="#">
                        <div class="parent-icon"><i class="bx bx-home-alt"></i>
                        </div>
                        <div class="menu-title">TOWS</div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="parent-icon"><i class="lni lni-tshirt"></i>
                        </div>
                        <div class="menu-title">Produk TOWS</div>
                    </a>
                </li>
                <li class="menu-label">Reseller Menu</li>
                <li>
                    <a href="#">
                        <div class="parent-icon"><i class="lni lni-tshirt"></i>
                        </div>
                        <div class="menu-title">Produk Fresh Factory</div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="parent-icon"><i class="bi bi-book"></i>
                        </div>
                        <div class="menu-title">Katalog Saya</div>
                    </a>
                </li>
                <li class="menu-label">Pengaturan</li>
                <li>
                    <a href="#" target="_blank">
                        <div class="parent-icon"><i class="bi bi-link"></i>
                        </div>
                        <div class="menu-title">Integrasi Omnichannel</div>
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        <div class="parent-icon"><i class="lni lni-code-alt"></i>
                        </div>
                        <div class="menu-title">Kode Program</div>
                    </a>
                </li>
                <!-- <li>
                    <a href="#" target="_blank">
                        <div class="parent-icon"><i class="bx b-log-out"></i>
                        </div>
                        <div class="menu-title">Logout</div>
                    </a>
                </li> -->
            </ul>
            <!--end navigation-->
        </aside>
        <!--end sidebar -->