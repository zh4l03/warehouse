<?= include('layout/header.php') ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#example').dataTable( {
            "pagingType": "scrolling"
        } );
    } );
</script>

<!--start content-->
<main class="page-content">
  <!--breadcrumb-->
  <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
    <div class="breadcrumb-title pe-3">Produk</div>
    <div class="ps-3">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-0 p-0">
          <li class="breadcrumb-item"><a href="javascript:;"><i class="lni lni-tshirt"></i></a>
          </li>
          <li class="breadcrumb-item active" aria-current="page">Tambah Produk Utama</li>
        </ol>
      </nav>
    </div>
  </div>
  <!--end breadcrumb-->
  <div class="row">
    <div class="col-12 col-lg-12 col-xl-12 d-flex">
      <div class="card radius-10 w-100">
        <div class="card-body">
          <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 row-cols-xl-4 row-cols-xxl-4 g-4">
            <div class="col-12">
              <table width="100%">
                <tr>
                  <td>
                    <h3><label for="validationCustom04" class="form-label">Cari Berdasarkan</label></h3>
                  </td>
                  <td style="text-align: right;">
                    <a href="<?= base_url('produk/tambahprodukutama') ?>" class="btn btn-sm btn-success px-2"><i class="lni lni-circle-plus"></i> Tambah Produk </a>
                    <a href="#" class="btn btn-sm btn-primary px-2"><i class="lni lni-share-alt"></i> Export </a>
                    <a href="#" class="btn btn-sm btn-primary px-2"><i class="lni lni-download"></i> Import </a>
                  </td>
                </tr>
              </table>
            </div>
            <div class="col select2-sm">
              <!-- <label for="validationCustom04" class="form-label">Tempat Penyimpanan</label> -->
              <select class="single-select">
                <option value="">.: Berdasarkan :.</option>
                <option value="">Nama Produk</option>
                <option value="">Kode SKU</option>
                <option value="">Zona</option>
              </select>
            </div>
            <div class="col select2-sm">
              <!-- <label for="validationCustom04" class="form-label">Tempat Penyimpanan</label> -->
              <select class="single-select">
                <option value="">.: Pilih Kategori Produk :.</option>
                <option value="">Buah-buahan</option>
                <option value="">Daging</option>
                <option value="">Es</option>
              </select>
            </div>
            <div class="col select2-sm">
              <!-- <label for="validationCustom04" class="form-label">Tempat Penyimpanan</label> -->
              <select class="single-select">
                <option value="">.: Pilih Tipe Produk :.</option>
                <option value="">Primary</option>
                <option value="">Secondary</option>
              </select>
            </div>
            <div class="col">
              <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Cari Produk" aria-label="Recipient's username" aria-describedby="button-addon2">
                <button class="btn btn-outline-secondary" type="submit"><i class='bx bx-search'></i></button>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-12 col-lg-12 col-xl-12 d-flex">
      <div class="card radius-10 w-100">
        <div class="card-body">
          <div class="col">
          <div class="table-responsive">
							<table id="example" class="table table-striped table-bordered" style="width:100%">
								<thead>
									<tr>
										<th>No</th>
										<th>Kode SKU</th>
										<th>Nama Produk</th>
										<th>Gambar</th>
										<th>Kategori Produk</th>
										<th>Berat</th>
										<th>Ukuran Loker</th>
										<th>Zona</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><a href="#">1</a></td>
										<td><a href="#">0703</a></td>
										<td><a href="#">Ceylon Blacktea</a></td>
										<td><a href="#"><img src="<?= base_url('images/products/01.png') ?>" class="product-img-2" alt="product img"></a></td>
										<td><a href="#">Lainnya</a></td>
										<td><a href="#">600 gr</a></td>
										<td><a href="#">Tipis - Besar</a></td>
										<td><a href="#">Kering</a></td>
										<td>
                        <div class="btn-group">
                          <button type="button" class="btn btn-success btn-sm"><i class="lni lni-pencil-alt"></i></button>
                          <button type="button" class="btn btn-danger btn-sm"><i class="bi bi-trash"></i></button>
                        </div>
                    </td>
									</tr>
									<tr>
										<td><a href="#">2</a></td>
										<td><a href="#">0704</a></td>
										<td><a href="#">Ceylon Blackpink</a></td>
										<td><a href="#"><img src="<?= base_url('images/products/02.png') ?>" class="product-img-2" alt="product img"></a></td>
										<td><a href="#">Lainnya</a></td>
										<td><a href="#">600 gr</a></td>
										<td><a href="#">Tipis - Besar</a></td>
										<td><a href="#">Kering</a></td>
										<td>
                        <div class="btn-group">
                          <button type="button" class="btn btn-success btn-sm"><i class="lni lni-pencil-alt"></i></button>
                          <button type="button" class="btn btn-danger btn-sm"><i class="bi bi-trash"></i></button>
                        </div>
                    </td>
									</tr>
									<tr>
										<td><a href="#">3</a></td>
										<td><a href="#">0705</a></td>
										<td><a href="#">Ceylon Greentea</a></td>
										<td><a href="#"><img src="<?= base_url('images/products/03.png') ?>" class="product-img-2" alt="product img"></a></td>
										<td><a href="#">Lainnya</a></td>
										<td><a href="#">600 gr</a></td>
										<td><a href="#">Tipis - Besar</a></td>
										<td><a href="#">Kering</a></td>
										<td>
                        <div class="btn-group">
                          <button type="button" class="btn btn-success btn-sm"><i class="lni lni-pencil-alt"></i></button>
                          <button type="button" class="btn btn-danger btn-sm"><i class="bi bi-trash"></i></button>
                        </div>
                    </td>
									</tr>
								</tbody>
								<!-- <tfoot>
									<tr>
										<th>Name</th>
										<th>Position</th>
										<th>Office</th>
										<th>Age</th>
										<th>Start date</th>
										<th>Salary</th>
									</tr>
								</tfoot> -->
							</table>
						</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--end row-->
</main>
<!--end page main-->

<?= include('layout/footer.php') ?>