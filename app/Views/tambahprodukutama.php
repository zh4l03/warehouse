<?= include('layout/header.php') ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#example').dataTable({
            "pagingType": "scrolling"
        });
    });
</script>

<!--start content-->
<main class="page-content">
    <!--breadcrumb-->
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">Produk</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="lni lni-tshirt"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Produk Utama</li>
                </ol>
            </nav>
        </div>
    </div>
    <!--end breadcrumb-->
    <div class="row">
        <div class="col-12 col-lg-12 col-xl-12 d-flex">
            <div class="card radius-10 w-100">
                <div class="card-body">
                    <div class="row">
                        <div class="border p-3 rounded">
                            <h6 class="mb-0 text-uppercase">Tambah Produk Utama</h6>
                            <i style="color: red;">* Harus diisi</i>
                            <hr />
                            <form class="row g-3">
                                <div class="col-12">
                                    <label class="form-label">Kode SKU <i style="color: red;">*</i></label>
                                    <input type="text" class="form-control" placeholder="Kode SKU">
                                </div>
                                <div class="col-12">
                                    <label class="form-label">Foto Produk <i style="color: red;">*</i></label>
                                    <input type="file" class="form-control">
                                </div>
                                <div class="col-12">
                                    <label class="form-label">Foto Cantik <i style="color: red;">*</i></label>
                                    <input type="file" class="form-control">
                                </div>
                                <div class="col-12">
                                    <label class="form-label">Nama Produk <i style="color: red;">*</i></label>
                                    <input type="text" class="form-control" placeholder="Nama Produk">
                                </div>
                                <div class="col-6">
                                    <div class="col select2-sm">
                                        <label class="form-label">Zona <i style="color: red;">*</i></label>
                                        <select class="single-select">
                                            <option value="">.: Pilih Zona :.</option>
                                            <option value="">Beku</option>
                                            <option value="">Sejuk</option>
                                            <option value="">Kering</option>
                                            <option value="">Dingin</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6"></div>
                                <div class="col-6">
                                    <div class="col select2-sm">
                                        <label class="form-label">Kategori Produk <i style="color: red;">*</i></label>
                                        <select class="single-select">
                                            <option value="">.: Pilih Kategori Produk :.</option>
                                            <option value="">Buah-buahan</option>
                                            <option value="">Daging</option>
                                            <option value="">Es</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label class="form-label">Berat (Grams) <i style="color: red;">*</i></label>
                                    <input type="number" class="form-control" placeholder="Berat (Dalam Grams)">
                                </div>
                                <div class="col-12">
                                    <div class="alert border-0 bg-light-danger alert-dismissible fade show py-1">
                                        <div class="d-flex align-items-center">
                                            <div class="ms-3">
                                                <div class="text-danger">Masukkan ukuran dengan tepat! *</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="ukuran" value="">
                                        <label class="form-check-label" for="inlineRadio1">Horizontal</label><br>
                                        <img src="<?= base_url('images/horizontal.png') ?>">
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="ukuran" value="">
                                        <label class="form-check-label" for="inlineRadio1">Vertikal</label><br>
                                        <img src="<?= base_url('images/vertikal.png') ?>">
                                    </div>
                                </div>
                                <div class="col-8"></div>
                                <div class="col-3">
                                    <label class="form-label">Panjang (cm) <i style="color: red;">*</i></label>
                                    <input type="text" class="form-control" placeholder="Panjang">
                                </div>
                                <div class="col-3">
                                    <label class="form-label">Lebar (cm) <i style="color: red;">*</i></label>
                                    <input type="text" class="form-control" placeholder="Lebar">
                                </div>
                                <div class="col-3">
                                    <label class="form-label">Tinggi (cm) <i style="color: red;">*</i></label>
                                    <input type="text" class="form-control" placeholder="Tinggi">
                                </div>
                                <div class="col-3">
                                    <br><br>
                                    <label>| Ukuran Loker</label>
                                    <!-- <input type="text" class="form-control" placeholder="Panjang"> -->
                                </div>
                                <div class="col-6">
                                    <div class="col select2-sm">
                                        <label class="form-label">Kemasan <i style="color: red;">*</i></label>
                                        <select class="single-select">
                                            <option value="">.: Pilih Kemasan :.</option>
                                            <option value="">Vacuum Bag</option>
                                            <option value="">Plastic Bag</option>
                                            <option value="">Lainnya</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label class="form-label">Umur Simpan (Hari) <i style="color: red;">*</i></label>
                                    <input type="number" class="form-control" placeholder="Umur Simpan (dalam Hari)">
                                </div>
                                <div class="col-6">
                                    <label class="form-label">Harga Beli Produk</label>
                                    <input type="text" class="form-control" placeholder="Harga Beli Produk">
                                </div>
                                <div class="col-6">
                                    <label class="form-label">Harga Jual Produk <i style="color: red;">*</i></label>
                                    <input type="text" class="form-control" placeholder="Harga Jual Produk">
                                </div>
                                <div class="col-12">
                                    <label class="form-label">Deskripsi Produk <i style="color: red;">*</i></label>
                                    <textarea class="form-control"></textarea>
                                </div>
                                <div class="col-2">
                                    <div class="d-grid">
                                        <button type="submit" class="btn btn-primary"><i class="lni lni-circle-plus"></i> Tambahkan</button>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="d-grid">
                                        <a href="../produk" class="btn btn-danger">Kembali</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end row-->
</main>
<!--end page main-->

<?= include('layout/footer.php') ?>